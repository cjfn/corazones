<?php 
    require 'db.php';
    $id = null;
    if(!empty($_GET['id']))
    {
        $id = $_GET['id'];
    }
    if($id == null)
    {
        header("Location: agregar mascota.php");
    }
    else
    {
        // read data
        $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM mascotas where id_mascota = ?";
        $stmt = $PDO->prepare($sql);
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $PDO = null;
        if (empty($data)){
            header("Location: agregar mascota.php");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>
 
<body>
<div class="container">
    <div class="col-sm-12">
        <div class="row">
            <h3>Read a User</h3>
        </div>
            
        <div class="form-group col-sm-12 col-lg-12">
            <label class="col-sm-2 control-label">Nombre</label>
            <div class="col-sm-10">
              <p class="form-control-static"><?php echo $data['nombre'];?></p>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="col-sm-2 control-label">tipo de mascota</label>
            <div class="col-sm-10">
              <p class="form-control-static"><?php echo $data['tipo_mascota'];?></p>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="col-sm-2 control-label">raza</label>
            <div class="col-sm-10">
              <p class="form-control-static"><?php echo $data['raza'];?></p>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="col-sm-2 control-label">Comportamiento:</label>
            <div class="col-sm-10">
              <p class="form-control-static"><?php echo $data['comportamiento'];?></p>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <a class="btn btn btn-default" href="agregar mascota.php">Back</a>
        </div>
    </div>                
</div>
</body>
</html>